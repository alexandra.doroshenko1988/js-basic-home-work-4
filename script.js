let firstNum
let secondNum
let mathOperator

do {
    firstNum = prompt('Enter first number', firstNum ? firstNum : '');
    secondNum = prompt('Enter second number', secondNum ? secondNum : '');
} while (!firstNum || isNaN(firstNum) || !secondNum || isNaN(secondNum))

do {
    mathOperator = prompt('Enter Math Operator', '/ * + -')
} while (!mathOperator)

function countUserNum (first, second, operator) {
    if (operator === '*') {
        console.log(first * second);
    }
    if (operator === '+') {
        console.log(+first + +second);
    }
    if (operator === '-') {
        console.log(first - second);
    }
    if (operator === '/') {
        console.log(first / second);
    }    
}
countUserNum(firstNum, secondNum, mathOperator)